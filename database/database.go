package database

import "database/sql"

type Database interface {
	Session() (*sql.DB, error)
	Must(db *sql.DB, err error) *sql.DB
}
