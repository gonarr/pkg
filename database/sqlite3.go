package database

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type sqlite3Database struct {
	databaseName string
}

func NewSqlite3Database(databaseName string) Database {
	return &sqlite3Database{
		databaseName: databaseName,
	}
}

func (s *sqlite3Database) Session() (*sql.DB, error) {
	return sql.Open("sqlite3", s.databaseName)
}

func (s *sqlite3Database) Must(db *sql.DB, err error) *sql.DB {
	if err != nil {
		panic(err.Error())
	}

	return db
}
