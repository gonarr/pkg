package filesystem

import (
	"strings"
)

// DirPath always ends with a slash /
type DirPath string

func CreateDirPath(dirPath string) DirPath {
	if !strings.HasSuffix(dirPath, "/") {
		return DirPath(dirPath + "/")
	}

	return DirPath(dirPath)
}

func (d DirPath) String() string {
	return string(d)
}

func (d DirPath) DirName() string {
	splitPath := strings.Split(d.String(), "/")
	return splitPath[len(splitPath)-2]
}

type FilePath struct {
	directoryPath DirPath
	fileName      string
}

func FilePathFromString(file string) FilePath {
	splitPath := strings.Split(file, "/")

	fileName := splitPath[len(splitPath)-1]
	dir := strings.Join(splitPath[:len(splitPath)-1], "/")

	return CreateFilePath(CreateDirPath(dir), fileName)
}

func CreateFilePath(dirPath DirPath, filename string) FilePath {
	return FilePath{
		directoryPath: dirPath,
		fileName:      filename,
	}
}

func (f FilePath) String() string {
	return f.directoryPath.String() + f.fileName
}

func (f FilePath) FileName() string {
	return f.fileName
}

func (f FilePath) FileExtension() string {
	splits := strings.Split(f.FileName(), ".")
	return splits[len(splits)-1]
}

func (f FilePath) Directory() DirPath {
	return f.directoryPath
}
