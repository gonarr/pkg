package filesystem_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gonarr/pkg/filesystem"
)

func TestDirName(t *testing.T) {
	path := "/this/is/the/name"
	expected := "/this/is/the/name/"
	dir := filesystem.CreateDirPath(path)
	if dir.String() != expected {
		t.Fatalf("Expected dir %s but got %s", expected, dir.String())
	}

	name := dir.DirName()
	if name != "name" {
		t.Fatalf("Expected dir name '%s' but got '%s'", "name", name)
	}
}

func TestFileName(t *testing.T) {
	path := "/this/is/the/name"
	expected := "file_name.txt"
	dir := filesystem.CreateDirPath(path)

	filePath := filesystem.CreateFilePath(dir, expected)
	if filePath.FileName() != "file_name.txt" {
		t.Fatalf("Expected file name %s but got %s", expected, filePath.FileName())
	}
}

func TestFileExtension(t *testing.T) {
	path := "/this/is/the/name"
	expected := "txt"
	dir := filesystem.CreateDirPath(path)

	filePath := filesystem.CreateFilePath(dir, "filename.txt")
	if filePath.FileExtension() != expected {
		t.Fatalf("Expected file name %s but got %s", expected, filePath.FileExtension())
	}
}

func TestFilePathFromString(t *testing.T) {
	tests := []struct {
		name                  string
		input                 string
		expectedDir           string
		expectedFileName      string
		expectedFileExtension string
	}{
		{
			name:                  "Happy case",
			input:                 "/this/is/dir/file.name",
			expectedDir:           "/this/is/dir/",
			expectedFileName:      "file.name",
			expectedFileExtension: "name",
		},
		{
			name:                  "Root dir",
			input:                 "/file.name",
			expectedDir:           "/",
			expectedFileName:      "file.name",
			expectedFileExtension: "name",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			filePath := filesystem.FilePathFromString(test.input)
			assert.Equal(t, test.expectedDir, filePath.Directory().String())
			assert.Equal(t, test.expectedFileName, filePath.FileName())
			assert.Equal(t, test.expectedFileExtension, filePath.FileExtension())
		})
	}
}
