package filesystem

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

type Mover interface {
	Move(src FilePath, destDir DirPath, name string) error
	Copy(src FilePath, destDir DirPath, name string) error
	Symlink(oldName, newName FilePath) error
}

type mover struct {
	log     logrus.FieldLogger
	scanner Scanner
}

func NewMover(log logrus.FieldLogger) Mover {
	return &mover{
		scanner: NewScanner(),
		log:     log,
	}
}

func (m *mover) Move(src FilePath, destDir DirPath, name string) error {
	err := m.Copy(src, destDir, name)
	if err != nil {
		return err
	}

	m.log.Debugf("Removing source %s", src.String())
	return os.Remove(src.String())
}

func (m *mover) Copy(src FilePath, destDir DirPath, name string) error {
	if _, err := os.Stat(src.String()); os.IsNotExist(err) {
		m.log.WithError(err).WithField("filePath", src.String()).Error("Could not find file")
		return err
	}
	exists, err := m.scanner.DirExistsOrCreate(destDir)
	if exists && err == nil {
		dest := CreateFilePath(destDir, name)
		m.log.WithFields(logrus.Fields{
			"source":      src.String(),
			"destination": dest.String(),
		}).Debugf("Copying file")

		err = m.copyFile(src, dest)
		if err != nil {
			return err
		}
	}

	return err
}

func (m *mover) Symlink(oldName, newName FilePath) error {
	return os.Symlink(oldName.String(), newName.String())
}

func (m *mover) copyFile(src, dest FilePath) error {
	srcFile, err := os.Open(src.String())
	defer srcFile.Close()
	if err != nil {
		return err
	}

	destFile, err := os.Create(dest.String())
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile)
	if err != nil {
		return err
	}

	err = destFile.Sync()
	if err != nil {
		return err
	}

	m.log.Debugf("Copying done")
	return nil
}
