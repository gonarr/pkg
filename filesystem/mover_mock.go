package filesystem

import (
	"errors"
)

type MoverMock struct {
	isError     bool
	src         string
	dest        string
	linkOldName string
	linkNewName string
}

func NewMoverMock(isError bool) *MoverMock {
	return &MoverMock{
		isError: isError,
	}
}

func (m *MoverMock) Copy(src FilePath, destDir DirPath, name string) error {
	if m.isError {
		return errors.New("MoverMock error")
	}

	m.src = src.String()
	m.dest = CreateFilePath(destDir, name).String()
	return nil
}

func (m *MoverMock) Move(src FilePath, destDir DirPath, name string) error {
	if m.isError {
		return errors.New("MoverMock error")
	}

	return m.Copy(src, destDir, name)
}

func (m *MoverMock) Symlink(oldName, newName FilePath) error {
	if m.isError {
		return errors.New("MoverMock error")
	}

	m.linkOldName = oldName.String()
	m.linkNewName = newName.String()

	return nil
}

func (m *MoverMock) GetMove() (string, string) {
	return m.src, m.dest
}

func (m *MoverMock) GetLink() (string, string) {
	return m.linkOldName, m.linkNewName
}

func (m *MoverMock) Clear() {
	m.src = ""
	m.dest = ""
	m.linkOldName = ""
	m.linkNewName = ""
}
