package filesystem

import (
	"fmt"
	"io/ioutil"
	"os"
)

type Scanner interface {
	ListDirs(path DirPath) ([]DirPath, error)
	ListFiles(path DirPath) ([]FilePath, error)
	DirExistsOrCreate(path DirPath) (bool, error)
}

type scanner struct {
}

func NewScanner() Scanner {
	return &scanner{}
}

func (s *scanner) ListDirs(path DirPath) ([]DirPath, error) {
	if _, err := os.Stat(path.String()); os.IsNotExist(err) {
		return []DirPath{}, fmt.Errorf("%s could not scan %s", err.Error(), path.String())
	}

	files, err := ioutil.ReadDir(path.String())
	if err != nil {
		return []DirPath{}, fmt.Errorf("%s could not read dir %s", err.Error(), path.String())
	}
	dirs := []DirPath{}
	for _, f := range files {
		if !f.IsDir() {
			continue
		}

		dirs = append(dirs, CreateDirPath(path.String()+f.Name()))
	}

	return dirs, nil
}

func (s *scanner) ListFiles(path DirPath) ([]FilePath, error) {
	if _, err := os.Stat(path.String()); os.IsNotExist(err) {
		return []FilePath{}, fmt.Errorf("%s could not scan %s", err.Error(), path.String())
	}

	files, err := ioutil.ReadDir(path.String())
	if err != nil {
		return []FilePath{}, fmt.Errorf("%s could not read dir %s", err.Error(), path.String())
	}
	realFiles := []FilePath{}
	for _, f := range files {
		if f.IsDir() {
			continue
		}

		realFiles = append(realFiles, CreateFilePath(path, f.Name()))
	}

	return realFiles, nil
}

func (s *scanner) DirExistsOrCreate(path DirPath) (bool, error) {
	info, err := os.Stat(path.String())
	if os.IsNotExist(err) {
		if e := os.MkdirAll(path.String(), 0755); e != nil {
			return false, e
		}
		return true, nil
	}

	return info.IsDir(), nil
}
