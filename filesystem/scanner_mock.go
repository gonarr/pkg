package filesystem

type ScannerMock struct {
	dirs  map[DirPath][]DirPath
	files map[DirPath][]FilePath
}

func NewScannerMock() *ScannerMock {
	return &ScannerMock{
		dirs:  map[DirPath][]DirPath{},
		files: map[DirPath][]FilePath{},
	}
}

func (m *ScannerMock) ListDirs(path DirPath) ([]DirPath, error) {
	return m.dirs[path], nil
}

func (m *ScannerMock) ListFiles(path DirPath) ([]FilePath, error) {
	return m.files[path], nil
}

func (m *ScannerMock) DirExistsOrCreate(path DirPath) (bool, error) {
	return true, nil
}

func (m *ScannerMock) AddDir(path DirPath, dir DirPath) {
	if _, ok := m.dirs[path]; !ok {
		m.dirs[path] = []DirPath{}
	}

	m.dirs[path] = append(m.dirs[path], dir)
}

func (m *ScannerMock) AddEmptyDir(path DirPath) {
	if _, ok := m.dirs[path]; ok {
		return
	}

	m.dirs[path] = []DirPath{}
}

func (m *ScannerMock) AddFile(path DirPath, file FilePath) {
	if _, ok := m.files[path]; !ok {
		m.files[path] = []FilePath{}
	}

	m.files[path] = append(m.files[path], file)
}
