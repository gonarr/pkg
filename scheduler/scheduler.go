package scheduler

import (
	"errors"
	"time"

	"github.com/sirupsen/logrus"
)

type ScheduleTime int64

const (
	TEN_SECONDS    ScheduleTime = 10
	THIRTY_SECONDS              = 30
	MINUTE                      = 60
	TEN_MINUTES                 = MINUTE * 10
	HOUR                        = MINUTE * 60
	DAY                         = HOUR * 24
)

var ErrCannotScheduleInThePast = errors.New("Cannot schedule task to run in the past")

type Scheduler interface {
	Every(task func() error, name string, scheduleTime ScheduleTime, runImmediately bool) error
	At(task func() error, name string, timestamp time.Time) error
	Stop() error
	ScheduledTasks() int
}

type scheduler struct {
	log          logrus.FieldLogger
	activeTimers []*time.Timer
}

func New(log logrus.FieldLogger) Scheduler {
	return &scheduler{
		log:          log,
		activeTimers: []*time.Timer{},
	}
}

func (s *scheduler) Every(task func() error, name string, scheduleTime ScheduleTime, runImmediately bool) error {
	go func() {
		timer := time.NewTimer(time.Until(time.Now().UTC().Add(time.Duration(scheduleTime) * time.Second)))
		s.activeTimers = append(s.activeTimers, timer)

		if runImmediately {
			err := task()
			if err != nil {
				s.log.WithError(err).WithField("name", name).Warn("Scheduled task finished with an error")
			}
		}

		for {
			<-timer.C
			err := task()
			if err != nil {
				s.log.WithError(err).WithField("name", name).Warn("Scheduled task finished with an error")
			}

			timer.Reset(time.Until(time.Now().UTC().Add(time.Duration(scheduleTime) * time.Second)))
		}

	}()

	return nil
}

func (s *scheduler) At(task func() error, name string, timestamp time.Time) error {
	now := time.Now().UTC()

	if timestamp.UTC().Before(now) {
		return ErrCannotScheduleInThePast
	}

	go func() {
		timer := time.NewTimer(time.Until(timestamp.UTC()))
		s.activeTimers = append(s.activeTimers, timer)
		<-timer.C

		err := task()
		if err != nil {
			s.log.WithError(err).WithField("name", name).Warn("Scheduled task finished with an error")
		}
	}()

	return nil
}

func (s *scheduler) Stop() error {
	for _, timer := range s.activeTimers {
		if !timer.Stop() {
			<-timer.C
		}
	}

	return nil
}

func (s *scheduler) ScheduledTasks() int {
	return len(s.activeTimers)
}
