package torrentclient

import (
	"gitlab.com/gonarr/pkg/filesystem"
)

type Client interface {
	List() ([]Torrent, error)
	Get(torrentIDs []string) ([]Torrent, error)
	AddTorrentFromFile(torrentFile filesystem.FilePath) (Torrent, error)
	AddTorrentFromMagnetLink(magnetLink string) (Torrent, error)
	AddTorrentAsBase64(torrent string) (Torrent, error)
}
