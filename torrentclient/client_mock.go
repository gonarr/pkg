package torrentclient

import (
	"fmt"

	"gitlab.com/gonarr/pkg/filesystem"
)

type ClientMock struct {
	torrents map[string]Torrent
	toAdd    *Torrent
}

func NewMock() *ClientMock {
	return &ClientMock{
		torrents: map[string]Torrent{},
		toAdd:    nil,
	}
}

func (c *ClientMock) List() ([]Torrent, error) {
	torrents := make([]Torrent, 0, len(c.torrents))

	for _, torrent := range c.torrents {
		torrents = append(torrents, torrent)
	}

	return torrents, nil
}

func (c *ClientMock) Get(torrentIDs []string) ([]Torrent, error) {
	torrents := []Torrent{}
	for _, id := range torrentIDs {
		torrents = append(torrents, c.torrents[id])
	}

	return torrents, nil
}

func (c *ClientMock) AddTorrentFromFile(torrentFile filesystem.FilePath) (Torrent, error) {
	if c.toAdd == nil {
		return Torrent{}, fmt.Errorf("ClientMock error, no torrent to add")
	}

	c.torrents[c.toAdd.ID] = *c.toAdd
	return *c.toAdd, nil
}

func (c *ClientMock) AddTorrentFromMagnetLink(magnetLink string) (Torrent, error) {
	if c.toAdd == nil {
		return Torrent{}, fmt.Errorf("ClientMock error, no torrent to add")
	}

	c.torrents[c.toAdd.ID] = *c.toAdd
	return *c.toAdd, nil
}

func (c *ClientMock) AddTorrentAsBase64(torrent string) (Torrent, error) {
	if c.toAdd == nil {
		return Torrent{}, fmt.Errorf("ClientMock error, no torrent to add")
	}

	c.torrents[c.toAdd.ID] = *c.toAdd
	return *c.toAdd, nil
}

func (c *ClientMock) SetToAdd(torrent Torrent) {
	c.toAdd = &torrent
}
