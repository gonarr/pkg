package torrentclient

import "gitlab.com/gonarr/pkg/filesystem"

type Torrent struct {
	ID          string
	Name        string
	PercentDone float32
	Finished    bool
	DownloadDir filesystem.DirPath
	Files       []filesystem.FilePath
}
