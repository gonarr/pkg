package transmissionclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/gonarr/pkg/filesystem"
	"gitlab.com/gonarr/pkg/torrentclient"
)

// https://github.com/transmission/transmission/blob/master/libtransmission/transmission.h line 1651
const (
	TR_STATUS_STOPPED       int = 0 /* Torrent is stopped */
	TR_STATUS_CHECK_WAIT        = 1 /* Queued to check files */
	TR_STATUS_CHECK             = 2 /* Checking files */
	TR_STATUS_DOWNLOAD_WAIT     = 3 /* Queued to download */
	TR_STATUS_DOWNLOAD          = 4 /* Downloading */
	TR_STATUS_SEED_WAIT         = 5 /* Queued to seed */
	TR_STATUS_SEED              = 6 /* Seeding */
)

type transmissionClient struct {
	log          logrus.FieldLogger
	url          string
	username     string
	password     string
	sessionToken string
}

type torrentParams struct {
	Method    string           `json:"method"`
	Arguments torrentArguments `json:"arguments"`
}

type torrentArguments struct {
	Fields   []string `json:"fields,omitempty"`
	IDs      []int    `json:"ids,omitempty"`
	Filename string   `json:"filename,omitempty"`
	Paused   bool     `json:"paused,omitempty"`
	MetaInfo string   `json:"metainfo,omitempty"`
}

type transmissionTorrent struct {
	ID          int                `json:"id"`
	Name        string             `json:"name"`
	PercentDone float32            `json:"percentDone"`
	Status      int                `json:"status"`
	DownloadDir string             `json:"downloadDir"`
	Files       []transmissionFile `json:"files"`
	DoneDate    int64              `json:"doneDate"`
}

type transmissionFile struct {
	Name string `json:"name"`
}

type transmissionTorrentAdded struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func New(username, password, url string, log logrus.FieldLogger) torrentclient.Client {
	return &transmissionClient{
		log:      log,
		url:      url,
		username: username,
		password: password,
	}
}

func (t *transmissionClient) List() ([]torrentclient.Torrent, error) {
	return t.getTorrents([]int{})
}

func (t *transmissionClient) Get(torrentIDs []string) ([]torrentclient.Torrent, error) {
	ids := make([]int, 0, len(torrentIDs))
	for _, torrentID := range torrentIDs {
		if id, err := strconv.Atoi(torrentID); err == nil {
			ids = append(ids, id)
		} else {
			return []torrentclient.Torrent{}, fmt.Errorf("%s is not a valid transmission torrent id", torrentID)
		}
	}
	return t.getTorrents(ids)
}

func (t *transmissionClient) AddTorrentFromMagnetLink(magnetLink string) (torrentclient.Torrent, error) {
	return t.add(torrentArguments{
		Filename: magnetLink,
		Paused:   false,
	})
}

func (t *transmissionClient) AddTorrentFromFile(torrentFile filesystem.FilePath) (torrentclient.Torrent, error) {
	return t.add(torrentArguments{
		Filename: torrentFile.String(),
		Paused:   false,
	})
}

func (t *transmissionClient) AddTorrentAsBase64(torrent string) (torrentclient.Torrent, error) {
	return t.add(torrentArguments{
		MetaInfo: torrent,
		Paused:   false,
	})
}

func (t *transmissionClient) getTorrents(ids []int) ([]torrentclient.Torrent, error) {
	getParams := torrentParams{
		Method: "torrent-get",
		Arguments: torrentArguments{
			Fields: []string{
				"id",
				"name",
				"status",
				"downloadDir",
				"files",
				"doneDate",
				"percentDone",
			},
			IDs: ids,
		},
	}
	resp, err := t.doRequest(getParams)
	if err != nil {
		return []torrentclient.Torrent{}, err
	}
	if resp.StatusCode == 409 {
		t.sessionToken = resp.Header.Get("X-Transmission-Session-Id")
		resp.Body.Close()
		resp, _ = t.doRequest(getParams)
	}

	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		var responseBody struct {
			Arguments struct {
				Torrents []transmissionTorrent `json:"torrents"`
			} `json:"arguments"`
		}

		bodyString, _ := ioutil.ReadAll(resp.Body)
		err := json.Unmarshal(bodyString, &responseBody)
		if err != nil {
			return []torrentclient.Torrent{}, err
		}

		torrents := make([]torrentclient.Torrent, 0, len(responseBody.Arguments.Torrents))
		for _, torrent := range responseBody.Arguments.Torrents {
			downloadDir := filesystem.CreateDirPath(torrent.DownloadDir)

			files := make([]filesystem.FilePath, 0, len(torrent.Files))
			for _, file := range torrent.Files {
				files = append(files, filesystem.CreateFilePath(downloadDir, file.Name))
			}

			finished := false
			if torrent.DoneDate > 0 {
				finished = true
			} else if torrent.Status == 5 || torrent.Status == 6 {
				finished = true
			}
			torrents = append(torrents, torrentclient.Torrent{
				ID:          strconv.Itoa(torrent.ID),
				Name:        torrent.Name,
				PercentDone: torrent.PercentDone,
				Finished:    finished,
				DownloadDir: downloadDir,
				Files:       files,
			})
		}
		return torrents, nil
	}
	return []torrentclient.Torrent{}, nil
}

func (t *transmissionClient) add(arguments torrentArguments) (torrentclient.Torrent, error) {
	addParams := torrentParams{
		Method:    "torrent-add",
		Arguments: arguments,
	}
	resp, err := t.doRequest(addParams)
	if err != nil {
		return torrentclient.Torrent{}, err
	}
	if resp.StatusCode == 409 {
		t.sessionToken = resp.Header.Get("X-Transmission-Session-Id")
		resp.Body.Close()
		resp, _ = t.doRequest(addParams)
	}

	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		var responseBody struct {
			Arguments struct {
				TorrentAdded     *transmissionTorrentAdded `json:"torrent-added,omitempty"`
				TorrentDuplicate *transmissionTorrentAdded `json:"torrent-duplicate,omitempty"`
			} `json:"arguments"`
			Result string `json:"result"`
		}

		bodyString, _ := ioutil.ReadAll(resp.Body)
		err := json.Unmarshal(bodyString, &responseBody)
		if err != nil {
			return torrentclient.Torrent{}, err
		}

		if responseBody.Result != "success" {
			return torrentclient.Torrent{}, fmt.Errorf("Could not add torrent %s", responseBody.Result)
		}

		var addedTorrentID int
		if responseBody.Arguments.TorrentAdded != nil {
			addedTorrentID = responseBody.Arguments.TorrentAdded.ID
		} else if responseBody.Arguments.TorrentDuplicate != nil {
			addedTorrentID = responseBody.Arguments.TorrentDuplicate.ID
		} else {
			return torrentclient.Torrent{}, fmt.Errorf("Could not add torrent to client")
		}

		torrents, err := t.getTorrents([]int{addedTorrentID})
		if err != nil {
			return torrentclient.Torrent{}, fmt.Errorf("Could not add torrent to client %s", err.Error())
		}

		if len(torrents) != 1 {
			return torrentclient.Torrent{}, fmt.Errorf("Could not add torrent to client")
		}

		return torrents[0], nil
	}
	return torrentclient.Torrent{}, fmt.Errorf("Got non ok status code %d", resp.StatusCode)
}

func (t *transmissionClient) doRequest(params torrentParams) (*http.Response, error) {
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	t.log.Debugf("TransmissionClient POST %s Data: %s", t.url, string(body))
	client := &http.Client{}
	req, err := http.NewRequest("POST", t.url, bytes.NewBuffer(body))

	req.SetBasicAuth(t.username, t.password)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Transmission-Session-Id", t.sessionToken)

	return client.Do(req)
}
