package torrentleechsearcher

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

type TorrentLeechSearcher interface {
	Search(query string, categories ...Category) ([]TorrentLeechSearchResult, error)
	GetTorrent(torrent TorrentLeechSearchResult) (string, error)
}

type DownloadType string

const (
	DownloadTypeMagnet  DownloadType = "magnet"
	DownloadTypeTorrent              = "torrent"
)

type Category string

func (c Category) String() string {
	return string(c)
}

type TorrentLeechSearchResult struct {
	Name         string
	FileName     string
	Category     Category
	DownloadURL  string
	DownloadType DownloadType
	TVMazeID     string
	IMDBID       string
	Tags         []string
	Rating       float32
	Completed    int
	Seeders      int
}

type torrentLeechSearcher struct {
	log      logrus.FieldLogger
	username string
	password string
	cookies  []string
}

type searchResult struct {
	TorrentList []struct {
		FID       string          `json:"fid"`
		Filename  string          `json:"filename"`
		Name      string          `json:"name"`
		Category  int             `json:"categoryID"`
		TVMazeID  string          `json:"tvmazeID"`
		IMDBID    string          `json:"imdbID"`
		Tags      json.RawMessage `json:"tags"`
		Size      int             `json:"size"`
		Rating    float32         `json:"rating"`
		Completed int             `json:"completed"`
		Seeders   int             `json:"seeders"`
	} `json:"torrentList"`
}

const baseURL string = "https://www.torrentleech.org/"
const userAgentString = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362"
const (
	CATEGORY_TV_SHOW_HD      Category = "32"
	CATEGORY_TV_SHOW                  = "26"
	CATEGORY_MOVIE_HDRIP              = "43"
	CATEGORY_MOVIE_BLURAYRIP          = "14"
	CATEGORY_MOVIE_BLURAY             = "13"
	CATEGORY_MOVIE_4K                 = "47"
)

var ErrWrongContentTypeInResponse = errors.New("Got wrong content-type in response, might be login failure")

func New(username, password string, log logrus.FieldLogger) TorrentLeechSearcher {
	return &torrentLeechSearcher{
		log:      log,
		username: username,
		password: password,
		cookies:  []string{},
	}
}

func (t *torrentLeechSearcher) Search(query string, categories ...Category) ([]TorrentLeechSearchResult, error) {
	t.log.Debugf("Search for %s", query)
	if len(t.cookies) == 0 {
		t.login()
	}

	client := http.Client{}

	c := make([]string, len(categories))
	for i := range categories {
		c[i] = categories[i].String()
	}
	endPoint := fmt.Sprintf(
		"torrents/browse/list/categories/%s/query/%s/orderby/seeders/order/desc",
		strings.Join(c, ","),
		query,
	)
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", baseURL, endPoint), nil)
	if err != nil {
		return []TorrentLeechSearchResult{}, err
	}

	req.Header.Set("Cookie", strings.Join(t.cookies, ";"))
	resp, err := client.Do(req)
	if err != nil {
		return []TorrentLeechSearchResult{}, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK &&
		resp.Header.Get("Content-Type") == "application/json" {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return []TorrentLeechSearchResult{}, err
		}

		var data searchResult
		err = json.Unmarshal(body, &data)
		if err != nil {
			return []TorrentLeechSearchResult{}, err
		}

		resultTorrents := make([]TorrentLeechSearchResult, 0, len(data.TorrentList))
		for _, torrent := range data.TorrentList {
			tlsr := TorrentLeechSearchResult{
				Name:         torrent.Name,
				FileName:     torrent.Filename,
				Category:     Category(strconv.Itoa(torrent.Category)),
				DownloadURL:  fmt.Sprintf("%sdownload/%s/%s", baseURL, torrent.FID, torrent.Filename),
				DownloadType: DownloadTypeTorrent,
				TVMazeID:     strings.TrimLeft(torrent.TVMazeID, "e"),
				IMDBID:       torrent.IMDBID,
				Tags:         []string{},
				Rating:       torrent.Rating,
				Completed:    torrent.Completed,
				Seeders:      torrent.Seeders,
			}

			var tags []string
			if err := json.Unmarshal(torrent.Tags, &tags); err == nil {
				t.log.WithField("tags", tags).Debug("Is tags")
				tlsr.Tags = tags
			}

			resultTorrents = append(resultTorrents, tlsr)
		}

		return resultTorrents, nil
	}

	t.cookies = []string{}
	return []TorrentLeechSearchResult{}, ErrWrongContentTypeInResponse
}

func (t *torrentLeechSearcher) GetTorrent(torrent TorrentLeechSearchResult) (string, error) {
	return t.downloadTorrent(torrent.FileName, torrent.DownloadURL)
}

func (t *torrentLeechSearcher) login() {
	endPoint := "user/account/login/"

	form := url.Values{}
	form.Add("username", t.username)
	form.Add("password", t.password)

	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s", baseURL, endPoint), strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	for k, v := range resp.Header {
		if k == "Set-Cookie" {
			for _, c := range v {
				t.cookies = append(t.cookies, strings.Split(c, ";")[0])
			}
		}
	}

	t.cookies = append(t.cookies, userAgentString)
}

func (t *torrentLeechSearcher) downloadTorrent(name, url string) (string, error) {
	if len(t.cookies) == 0 {
		t.login()
	}

	client := http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Set("Cookie", strings.Join(t.cookies, ";"))
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}
